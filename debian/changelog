libscope-upper-perl (0.32-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:31:15 +0200

libscope-upper-perl (0.32-1) unstable; urgency=medium

  * Team upload.
  * Update upstream Git repository URL.
  * Import upstream version 0.32
  * Update upstream copyright years.
  * Bump debhelper compatibility version to 12.
  * Declare compatibility with Debian Policy 4.4.0.
  * Make dependencies provided by perl explicit.
  * Enable all compile-time hardening options.

 -- intrigeri <intrigeri@debian.org>  Wed, 24 Jul 2019 00:45:30 +0000

libscope-upper-perl (0.31-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.31.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.2.1.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Fri, 07 Sep 2018 20:11:20 +0200

libscope-upper-perl (0.30-1) unstable; urgency=medium

  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Import upstream version 0.30.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.1.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 Nov 2017 20:21:40 +0100

libscope-upper-perl (0.29-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Import upstream version 0.29.
    Fixes "FTBFS with Perl 5.24: 'CXt_LOOP_FOR' undeclared"
    (Closes: #825010)
  * Set bindnow linker flag in debian/rules.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Jun 2016 23:35:11 +0200

libscope-upper-perl (0.28-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.28

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Thu, 01 Oct 2015 15:24:29 -0300

libscope-upper-perl (0.27-1) unstable; urgency=medium

  * Import upstream version 0.27.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Jul 2015 22:13:54 +0200

libscope-upper-perl (0.25-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Import upstream version 0.25
  * Add debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Tue, 14 Oct 2014 20:22:24 +0200

libscope-upper-perl (0.24-1) unstable; urgency=low

  * New upstream release.
  * Drop build dependencies that are not required anymore.
  * Drop unused lintian override (hardening-no-fortify-functions).

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Oct 2013 16:39:37 +0200

libscope-upper-perl (0.22-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update years of copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 18 Feb 2013 18:58:58 +0100

libscope-upper-perl (0.21-1) unstable; urgency=low

  * New upstream release.
  * Update years of packaging copyright.
  * Bump Standards-Version to 3.9.4 (no changes).
  * Use debhelper 9.20120312 to get all hardening flags.
  * Add a lintian override for hardening-no-fortify-functions.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Dec 2012 18:42:42 +0100

libscope-upper-perl (0.19-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Update years of copyright.
  * debian/copyright: Use copyright format 1.0.
  * Bumped Standards-Version to 3.9.3.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 09 Sep 2012 13:18:47 +0200

libscope-upper-perl (0.18-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 14 Oct 2011 20:34:52 +0200

libscope-upper-perl (0.17-1) unstable; urgency=low

  * New upstream release.
  * Remove spelling patch, not required anymore.

 -- gregor herrmann <gregoa@debian.org>  Wed, 05 Oct 2011 19:02:48 +0200

libscope-upper-perl (0.16-1) unstable; urgency=low

  * New upstream release.
  * Add a patch for "allows (one) to".

 -- gregor herrmann <gregoa@debian.org>  Sun, 04 Sep 2011 15:44:39 +0200

libscope-upper-perl (0.15-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 29 Aug 2011 13:59:09 +0200

libscope-upper-perl (0.14-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release (closes: #628498).

  [ gregor herrmann ]
  * Update years of upstream and packaging copyright.
  * Bump debhelper compatibility level to 8.
  * Set Standards-Version to 3.9.2 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 Jun 2011 17:19:59 +0200

libscope-upper-perl (0.13-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Refer to /usr/share/common-licenses/GPL-1; refer to
    "Debian systems" instead of "Debian GNU/Linux systems".
  * Bump Standards-Version to 3.9.1.
  * Update my email address.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 21 Dec 2010 10:59:05 +0100

libscope-upper-perl (0.12-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 19 May 2010 19:52:03 +0900

libscope-upper-perl (0.11-1) unstable; urgency=low

  * New upstream release.
  * Remove patch: obsoleted by upstream changes.
  * Use source format 3.0 (quilt); drop README.source and quilt framework.
  * Bump Standards-Version to 3.8.4 (no changes).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 17 Apr 2010 20:20:24 +0900

libscope-upper-perl (0.10-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Added myself to Uploaders and Copyright
  * Refresh copyright to DEP5 format
  * Standards-Version 3.8.3 (no changes)
  * Use new short debhelper rules format
  * Add an override to fix shebangs in examples
  * Add a patch to correct POD spelling errors

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ gregor herrmann ]
  * Add libtest-portability-files-perl to Build-Depends.

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 19 Jan 2010 21:21:35 -0500

libscope-upper-perl (0.08-1) unstable; urgency=low

  * Initial release (closes: #524706).

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Apr 2009 13:46:34 +0200
